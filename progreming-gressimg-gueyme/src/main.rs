// Imports
use rand::Rng;
use std::cmp::Ordering;
use std::io;

// Main function
fn main() {
    // Show text on the screen
    println!(
        "
========================================================
   mmm                                #
 m'   ' m   m   mmm    mmm    mmm   mmm    m mm    mmmm
 #   mm #   #  #'  #  #   '  #   '    #    #'  #  #' '#
 #    # #   #  #''''   ''''   '''m    #    #   #  #   #
  'mmm' 'mm'#  '#mm   'mmm'  'mmm'  mm#mm  #   #  '#m'#
                                                   m  #
                                                    ##
   mmm
 m'   '  mmm   mmmmm   mmm
 #   mm '   #  # # #  #'  #
 #    # m'''#  # # #  #'''
  'mmm' 'mm'#  # # #  '#mm'
========================================================
    "
    );

    // Generates a secret number
    let secret_number = rand::thread_rng().gen_range(1, 101);

    // Ask your guess
    println!("Coloque seu chute:");

    // Tell the secrett number
    // println!("The secret number is: {}", secret_number);

    // Loops guesses until you guess the number
    loop {
        // Creates the var "guess"
        let mut guess = String::new();

        //Checks if the line is readable
        io::stdin()
            .read_line(&mut guess)
            .expect("Falha ao ler linha");

        //Handling invalid inputs
        let guess: u8 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => {
                println!("Digite um numero valido, puta mongol do caralho!");
                continue;
            }
        };

        // Checks your guess
        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Muito Pequeno!"),
            Ordering::Greater => println!("Muito Grande"),
            Ordering::Equal => {
                println!("Acertou!");
                break;
            }
        }
    }
}
