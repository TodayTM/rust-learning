fn main() {
    // Variables, Unmutable by default.
    let x = 5;
    println!("The value of x is: {}", x);

    // CAN'T DO THAT:
    // x = 6;
    // println!("The value of x is: {}", x);

    // Mutable Variables
    let mut y = 5;
    println!("The value of y is: {}", y);
    y = 6;
    println!("The value of y is: {}", y);

    // Constants
    const MAX_POINTS: u32 = 100_000;

    // Shadowing, different from mutable
    let z = 5;

    let z = z + 1;

    let z = z * 2;

    println!("The value of z is: {}", z);

    let spaces = "   ";
    let spaces = spaces.len();

    // CAN'T DO THAT:
    // let mut spaces = "   ";
    // spaces = spaces.len();
}
